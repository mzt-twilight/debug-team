package com.debugteam;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.debugteam.mapper")
public class DebugteamApplication {

    public static void main(String[] args) {
        SpringApplication.run(DebugteamApplication.class, args);
    }

}
