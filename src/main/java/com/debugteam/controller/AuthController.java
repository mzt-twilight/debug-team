package com.debugteam.controller;

import com.debugteam.model.bo.LoginDataBo;
import com.debugteam.model.vo.BaseRespVo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Map;

/**
 * @Classname AuthController
 * @Description NULL
 * @Date 2021/5/8 18:26
 * @Created by virus
 */
@Controller
@RequestMapping("admin/auth")
public class AuthController {

    /**
     * 先硬编码，之后需要连接数据库，之后进行SSH加密，传出token
     * @param map 接收前端传来的登录信息
     * @return 返回给前端token
     */
    @RequestMapping("login")
    @ResponseBody
    public BaseRespVo login(@RequestBody Map map) {
        String  username = (String) map.get("username");
        String  password = (String) map.get("password");
        // TODO 连接数据库查询校验操作
        return BaseRespVo.ok(username);
    }

    /**
     * 先硬编码，获得最大权限，之后需进行改进
     * @param token 用户唯一标识符，之后需进行SSH解密
     * @return 返回用户登录信息
     */
    @RequestMapping("info")
    @ResponseBody
    public BaseRespVo info(String token) {
        ArrayList<String > rolesList = new ArrayList<>();
        rolesList.add("超级管理员");
        ArrayList<String> permsList = new ArrayList<>();
        permsList.add("*");
        LoginDataBo loginDataBo = new LoginDataBo("admin123", "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif", rolesList, permsList);
        return BaseRespVo.ok(loginDataBo);
    }
}
