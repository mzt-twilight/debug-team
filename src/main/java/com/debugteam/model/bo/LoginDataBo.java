package com.debugteam.model.bo;

import java.util.List;

/**
 * @Classname LoginDataBo
 * @Description NULL
 * @Date 2021/5/8 18:35
 * @Created by virus
 */
public class LoginDataBo {


    /**
     * roles : ["超级管理员"]
     * name : admin123
     * perms : ["*"]
     * avatar : https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif
     */

    private String name;
    private String avatar;
    private List<String> roles;
    private List<String> perms;

    public LoginDataBo() {
    }

    public LoginDataBo(String name, String avatar, List<String> roles, List<String> perms) {
        this.name = name;
        this.avatar = avatar;
        this.roles = roles;
        this.perms = perms;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public List<String> getPerms() {
        return perms;
    }

    public void setPerms(List<String> perms) {
        this.perms = perms;
    }
}
