package com.debugteam.model.vo;

/**
 * @Classname BaseRespVo
 * @Description 直接与前端交互的Vo对象
 * @Date 2021/5/8 18:08
 * @Created by virus
 */
public class BaseRespVo<T> {

    private T data;
    private String errmsg;
    private Integer errno;

    private BaseRespVo() {
    }

    private BaseRespVo(T data, String errmsg, Integer errno) {
        this.data = data;
        this.errmsg = errmsg;
        this.errno = errno;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public Integer getErrno() {
        return errno;
    }

    public void setErrno(Integer errno) {
        this.errno = errno;
    }

    public static <T> BaseRespVo ok() {
        return new BaseRespVo<T>(null, "成功", 0);
    }

    public static <T> BaseRespVo ok(T data) {
        return new BaseRespVo<T>(data, "成功", 0);
    }

    public static <T> BaseRespVo ok(T data, String errmsg) {
        return new BaseRespVo<T>(data, errmsg, 0);
    }

    public static <T> BaseRespVo error() {
        return new BaseRespVo<T>(null, "失败", 500);
    }

    public static <T> BaseRespVo error(String errmsg) {
        return new BaseRespVo<T>(null, errmsg, 500);
    }

    public static <T> BaseRespVo error(String errmsg, Integer errno) {
        return new BaseRespVo<T>(null, errmsg, errno);
    }
}
